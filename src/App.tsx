import { FC, Suspense } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import CommonRoutes from './app/Routes/CommonRoutes';
import { store } from './app/store';
import DropdownProvider from './features/Common/Components/Dropdown/DropdownProvider';
import DefaultLayout from './features/Common/Layout/DefaultLayout';
import './index.scss';

import './locales/config';

const App: FC = () => {
  return (
    <Suspense fallback="loading">
      <Provider store={store}>
        <BrowserRouter>
          <DefaultLayout>
            <CommonRoutes />
          </DefaultLayout>
          <DropdownProvider />
        </BrowserRouter>
      </Provider>
    </Suspense>
  );
};

export default App;
