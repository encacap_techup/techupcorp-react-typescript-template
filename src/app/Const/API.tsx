const AUTHENTICATION_API_PATH = {
  LOGIN_PATH: '/auth/login',
};

export { AUTHENTICATION_API_PATH };
