const DEFAULT_PAGE_SIZE = 10;
const DEBOUNCE_CALL_API_TIME = 800;

export { DEFAULT_PAGE_SIZE, DEBOUNCE_CALL_API_TIME };
