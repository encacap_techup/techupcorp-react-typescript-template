const AUTHENTICATION_PATH = {
  LOGIN_PATH: '/auth/login',
};

const MY_PATH = {
  HOME_PATH: '/my',
};

const ADMIN_PATH = {
  HOME_PATH: '/admin',
};

const SYSTEM_PATH = {
  HOME_PATH: '/system',
};

export { AUTHENTICATION_PATH, MY_PATH, ADMIN_PATH, SYSTEM_PATH };
