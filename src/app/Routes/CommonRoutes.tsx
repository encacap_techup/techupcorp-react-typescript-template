import _ from 'lodash';
import { FC, useLayoutEffect, useState } from 'react';
import { matchPath, Route, Routes, useLocation, useNavigate } from 'react-router-dom';

import AuthRoutes from '../../features/Common/Auth/Routes/AuthRoutes';
import LoadingOverlay from '../../features/Common/Components/Loading/LoadingOverlay';
import useDispatch from '../../features/Common/Hooks/useDispatch';
import useSelector from '../../features/Common/Hooks/useSelector';
import DocumentationRoutes from '../../features/Documentation/Routes/DocumentationRoutes';
import Home from '../../features/Home/Home';
import { AUTHENTICATION_PATH } from '../Const/URL';
import { AuthService } from '../Services';
import { setUser } from '../Slices/userSlice';
import PrivateRoutes from './PrivateRoutes';

const CommonRoutes: FC = () => {
  const [isLoading, setIsLoading] = useState(true);

  const user = useSelector((state) => state.user);
  const location = useLocation();
  const excludeRedirectPaths = ['/', 'error/*', 'auth/*'];
  const excludeGetUserPaths = ['auth/*'];

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useLayoutEffect(() => {
    if (user.uuid !== null && user.uuid !== 0) {
      setIsLoading(false);
      return;
    }

    const isMatchedExcludeRedirectPath = excludeRedirectPaths.some((path) =>
      matchPath(path, location.pathname),
    );
    const isMatchedGetUserExcludePath = excludeGetUserPaths.some((path) =>
      matchPath(path, location.pathname),
    );

    if (isMatchedGetUserExcludePath) {
      setIsLoading(false);
      return;
    }

    if (_.isEmpty(user.email) || user.email === '') {
      AuthService.getMe()
        .then((data) => {
          return dispatch(setUser(data));
        })
        .catch(() => {
          if (isMatchedExcludeRedirectPath) {
            return;
          }
          navigate(AUTHENTICATION_PATH.LOGIN_PATH);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [location, user]);

  return isLoading ? (
    <LoadingOverlay />
  ) : (
    <Routes>
      <Route path="*" element={<Home />} />
      <Route
        path="admin/*"
        element={
          <PrivateRoutes>
            <div className="flex h-fit-layout items-center justify-center">Admin</div>
          </PrivateRoutes>
        }
      />
      <Route
        path="my/*"
        element={
          <PrivateRoutes>
            <div className="flex h-fit-layout items-center justify-center">My</div>
          </PrivateRoutes>
        }
      />
      <Route path="docs/*" element={<DocumentationRoutes />} />
      <Route path="auth/*" element={<AuthRoutes />} />
    </Routes>
  );
};

export default CommonRoutes;
