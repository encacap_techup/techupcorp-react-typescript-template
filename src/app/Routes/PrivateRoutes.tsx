import _ from 'lodash';
import { useLayoutEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

import NotFoundError from '../../features/Common/Error/Components/NotFoundError';
import useSelector from '../../features/Common/Hooks/useSelector';
import { UserRoleDataType } from '../Types/Common/userTypes';

interface PrivateRoutesProps {
  children: JSX.Element;
}

const PrivateRoutes = ({ children }: PrivateRoutesProps): JSX.Element => {
  const [hasPermission, setHasPermission] = useState(false);

  const user = useSelector((state) => state.user);
  let currentUrlPrefix = useLocation().pathname.split('/')[1];

  const checkPermissionByRoles = (roles: UserRoleDataType[]) => {
    if (currentUrlPrefix === 'my') currentUrlPrefix = 'user';

    if (Array.isArray(roles) && roles.length === 0 && currentUrlPrefix === 'user') {
      setHasPermission(true);
      return;
    }

    if (roles.some((role) => role.slug === currentUrlPrefix)) {
      setHasPermission(true);
    } else {
      setHasPermission(false);
    }
  };

  useLayoutEffect(() => {
    if (!_.isEmpty(user.email)) {
      checkPermissionByRoles(user?.roles);
    }
  }, [user, currentUrlPrefix]);

  return hasPermission ? children : <NotFoundError />;
};

export default PrivateRoutes;
