import { TableColumnFiltersState } from '../../Types/Common/elementTypes';

interface ParamTypes {
  [key: string]: string | number | unknown[] | undefined;
}

const generateFilterString = (filterParams?: TableColumnFiltersState) => {
  return filterParams
    ?.map((filterParam) => {
      return `filter[${filterParam.filterBy}]=${filterParam.values.join(',')}`;
    })
    .join('&');
};

const generateSearchParams = (params: ParamTypes) => {
  const searchParams = new URLSearchParams();

  Object.keys(params).forEach((key) => {
    const value = params[key];

    if (value === undefined) {
      return;
    }

    if (typeof value === 'string' || typeof value === 'number') {
      searchParams.append(key, String(params[String(key)]));
      return;
    }

    if (Array.isArray(value)) {
      value.forEach((item) => {
        searchParams.append(key, item);
      });
    }
  });

  return searchParams;
};

const generateParamString = (params: ParamTypes, filterParams?: TableColumnFiltersState) => {
  const filterString = generateFilterString(filterParams);
  const searchParamsString = generateSearchParams(params).toString();

  if (filterParams !== undefined) {
    return `${searchParamsString}&${filterString ?? ''}`;
  }

  return searchParamsString;
};

export { generateSearchParams, generateParamString };
