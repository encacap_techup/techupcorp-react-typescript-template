import { BaseQueryParamsType, ResponseDataWithMetaType } from '../../Types/Common/commonTypes';
import { UserDataType } from '../../Types/Common/userTypes';

const getFakeData = (pageSize: number): UserDataType[] =>
  Array.from({ length: pageSize }).map((_, index) => ({
    uuid: index,
    username: `user${index}`,
    first_name: 'Trần Văn',
    last_name: 'E',
    email: `encacap_${index}@gmail.com`,
    add_by_uuid: 0,
    avatar_img: 'https://i.pinimg.com/474x/68/f0/93/68f093dd88a7753a738075954abfb101.jpg',
    cover_img: 'https://i.pinimg.com/474x/68/f0/93/68f093dd88a7753a738075954abfb101.jpg',
    roles: [
      {
        uuid: 1,
        name: 'admin',
        slug: 'admin',
      },
    ],
  }));

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const getTableExampleData = async (params?: BaseQueryParamsType) =>
  await new Promise<ResponseDataWithMetaType<UserDataType[]>>((resolve) => {
    setTimeout(() => {
      const fakeData = getFakeData(params?.perPage ?? 10);
      resolve({
        data: fakeData,
        meta: {
          current_page: 1,
          per_page: 4,
          total: fakeData.length,
          last_page: 1,
        },
      });
    }, 1000);
  });

export { getTableExampleData };
