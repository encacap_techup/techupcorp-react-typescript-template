export interface AuthTokensType {
  accessToken: string;
  refreshToken: string;
}

export interface AuthLoginType {
  email: string;
  password: string;
}

export interface AuthRegisterFormDataType {
  username: string;
  fullName: string;
  email: string | null;
  password: string;
  passwordConfirmation: string;
}
