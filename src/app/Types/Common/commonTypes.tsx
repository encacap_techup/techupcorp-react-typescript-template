import { PaginationState } from '@tanstack/react-table';
import { AxiosError, AxiosResponse } from 'axios';

import { TableColumnFiltersState } from './elementTypes';

export interface AxiosErrorMessageType {
  [key: string]: string[];
}

export interface AxiosErrorDataType {
  statusCode: number;
  message: string;
  errors: AxiosErrorMessageType[];
  code: string;
}

export type AxiosErrorType = AxiosError<AxiosErrorDataType>;

export interface TablePaginationType extends Omit<PaginationState, 'pageIndex' | 'pageSize'> {
  pageIndex?: number;
  pageSize?: number;
  totalRows?: number;
  totalPages?: number;
}

export interface ResponseMetaType {
  current_page: number;
  per_page: number;
  total: number;
  last_page: number;
}

export interface ResponseDataWithMetaType<T = unknown> extends Partial<AxiosResponse> {
  data: T;
  meta: ResponseMetaType;
}

export interface AxiosResponseType<T> extends AxiosResponse {
  statusCode: number;
  message: string;
  meta: ResponseMetaType;
  data: {
    data: T;
  };
}

export interface BaseQueryParamsType {
  page?: number;
  perPage?: number;
  type?: string;
  query?: string;
  filterBy?: string;
  filterValue?: string;
  filterParams?: TableColumnFiltersState;
}

export interface ImageDataType extends Partial<File> {
  slug: string;
  absolute_slug: string;
}

export type FormSchemaTranslationType<T> = Partial<{
  [key in keyof T]: Record<string, string>;
}>;
