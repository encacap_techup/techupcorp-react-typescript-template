export interface UserRoleDataType {
  uuid: number;
  name: string;
  slug?: string;
  deleted_at?: string;
  created_at?: string;
  updated_at?: string;
}

export interface UserConfigDataType {
  uuid: number;
  name: string;
  slug: string;
  deleted_at?: string;
  created_at?: string;
  updated_at?: string;
}

export interface UserDataType {
  uuid: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  roles: UserRoleDataType[];
  banned_at?: string;
  avatar_img?: string;
  avatar_img_absolute?: string;
  cover_img?: string;
  cover_img_absolute?: string;
  deleted_at?: string;
  created_at?: string;
  updated_at?: string;
}
