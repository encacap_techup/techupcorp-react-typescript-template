import { SingleValue } from 'react-select';

import { SelectOptionType } from '../Common/elementTypes';

export enum FormExampleGenderEnum {
  MALE = 'male',
  FEMALE = 'female',
}

export interface FormExampleFormDataType {
  name: string;
  email: string;
  enableSomething: boolean;
  enableWhenLoaded: boolean;
  otherEnableSomething: boolean;
  otherEnableWhenLoaded: boolean;
  gender: SingleValue<SelectOptionType> | null;
  multipleSelect: string[];
}
