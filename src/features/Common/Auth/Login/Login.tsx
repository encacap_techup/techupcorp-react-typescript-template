import { yupResolver } from '@hookform/resolvers/yup';
import { useLayoutEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';

import { AuthService } from '../../../../app/Services';
import { setUser } from '../../../../app/Slices/userSlice';
import Alert from '../../Components/Alert';
import Button from '../../Components/Button';
import { Input } from '../../Components/Form';
import { Logo } from '../../Components/Logo';
import useDispatch from '../../Hooks/useDispatch';
import { setDocumentTitle } from '../../Utils/helpers';
import { loginFormSchema } from '../Schemas/loginFormSchema';
import { generateRedirectURL } from '../Utils/generateRedirectURL';

interface LoginFormDataType {
  email: string;
  password: string;
}

interface LoginFormGeneralError {
  code: string;
  message: string;
}

const Login = () => {
  const { t } = useTranslation(['common'], {
    keyPrefix: 'common:pages.auth.pages.login',
  });

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [generalError, setGeneralError] = useState<LoginFormGeneralError | null>(null);
  const [searchParams] = useSearchParams();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    control,
    handleSubmit: useFormSubmit,
    watch,
    formState: { errors },
  } = useForm<LoginFormDataType>({
    resolver: yupResolver(loginFormSchema(t)),
  });

  const email = watch('email');

  const handleSubmit = useFormSubmit((data) => {
    setIsSubmitting(true);

    AuthService.loginWithEmailAndPassword(data.email, data.password)
      .then((userData) => {
        const redirectURL = generateRedirectURL(userData.roles, searchParams.get('redirect'));

        dispatch(setUser(userData));

        navigate(redirectURL);
      })
      .catch(() => {
        setGeneralError({
          code: 'auth/not-found',
          message: t('error.notFound'),
        });
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  });

  useLayoutEffect(() => {
    setDocumentTitle(t('pageTitle'));
  }, []);

  return (
    <div className="flex sm:pt-16 sm:pb-10">
      <div className="w-full max-w-screen-sm rounded-2xl border-gray-100 bg-white px-3 py-6 xs:px-4 sm:m-auto sm:border-2 sm:px-20 sm:py-12 sm:shadow-md">
        <Logo className="mx-auto mt-6 mb-12 flex h-14 items-center justify-center" />
        <div className="mt-4 md:text-center">
          <div className="text-xl font-bold sm:text-3xl">{t('title')}</div>
          <div className="mt-1 text-sm font-semibold text-gray-400 sm:mt-3 sm:text-base">{t('subtitle')}</div>
        </div>
        <form action="" className="mt-10 sm:mt-14" onSubmit={handleSubmit}>
          {generalError && (
            <Alert title={t('error.title')} message={generalError.message} type="error" className="mb-8">
              {generalError.code === 'auth/not-found' && (
                <Link
                  to={`/auth/register?email=${encodeURIComponent(email || '')}&redirect=${encodeURIComponent(
                    searchParams.get('redirect') ?? '',
                  )}`}
                >
                  {t('footer.createNow')}
                </Link>
              )}
            </Alert>
          )}
          <Input
            type="email"
            name="email"
            label={t('form.email.label')}
            className="w-full"
            control={control}
            error={errors.email?.message}
            disabled={isSubmitting}
          />
          <Input
            type="password"
            label={t('form.password.label')}
            name="password"
            className="mt-1 w-full md:mt-4 lg:mt-6"
            control={control}
            error={errors.password?.message}
            disabled={isSubmitting}
          />
          <div className="mt-3 flex justify-end sm:mb-0">
            <Link
              to="/auth/forgot-password"
              className="text-center text-sm font-semibold text-gray-400 hover:underline"
              role="link"
              tabIndex={-1}
            >
              {t('form.forgotPassword')}
            </Link>
          </div>
          <div className="box-border w-full">
            <Button
              type="submit"
              className="mt-4 block w-full"
              disabled={isSubmitting}
              isLoading={isSubmitting}
            >
              {t('form.loginButton')}
            </Button>
          </div>
        </form>
        <div className="text-center sm:mt-4">
          <div className="mt-8 mb-2 text-center sm:mt-14">
            {t('footer.doneHaveAccount')}{' '}
            <Link
              to={`/auth/register?redirect=${encodeURIComponent(searchParams.get('redirect') ?? '')}`}
              className="block font-semibold underline hover:text-primary-700 sm:inline-block"
            >
              {t('footer.createNow')}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
