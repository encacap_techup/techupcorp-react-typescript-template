import { TFunction } from 'i18next';
import { object, string } from 'yup';

const loginFormSchema = (t: TFunction) =>
  object().shape({
    email: string()
      .email(t('form.email.invalid') ?? '')
      .required(t('form.email.required') ?? ''),
    password: string().required(t('form.password.required') ?? ''),
  });

export { loginFormSchema };
