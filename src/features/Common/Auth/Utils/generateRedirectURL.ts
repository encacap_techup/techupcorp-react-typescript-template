import { ADMIN_PATH, MY_PATH, SYSTEM_PATH } from '../../../../app/Const/URL';
import { UserRoleDataType } from '../../../../app/Types/Common/userTypes';

const generateRedirectURL = (roles: UserRoleDataType[], forceRedirectURL?: string | null) => {
  if (forceRedirectURL) {
    return forceRedirectURL;
  }

  const roleSlugs = roles.map((role) => role.slug);

  if (roleSlugs.includes('system')) {
    return SYSTEM_PATH.HOME_PATH;
  }

  if (roleSlugs.includes('admin')) {
    return ADMIN_PATH.HOME_PATH;
  }

  return MY_PATH.HOME_PATH;
};

export { generateRedirectURL };
