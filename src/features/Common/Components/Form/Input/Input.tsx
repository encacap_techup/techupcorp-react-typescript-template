import { omit } from 'lodash';
import { ForwardedRef, forwardRef } from 'react';
import { Control, useController } from 'react-hook-form';

import UncontrolledInput, { UncontrolledInputProps } from './UncontrolledInput';

export type InputSizeType = 'xs' | 'sm' | 'normal';

export interface InputProps extends UncontrolledInputProps {
  name: string;
  control?: Control<any, any>;
}

const Input = ({ name, control, ...props }: InputProps, ref: ForwardedRef<HTMLInputElement>) => {
  if (!control || !control.register) {
    return <UncontrolledInput name={name} ref={ref} {...props} />;
  }

  const {
    field: { value = '', onChange, onBlur },
  } = useController({
    name,
    control,
  });

  return (
    <UncontrolledInput
      value={value ?? null}
      onChange={onChange}
      onBlur={onBlur}
      {...omit(props, ['value', 'onChange', 'onBlur'])}
    />
  );
};

export default forwardRef(Input);
