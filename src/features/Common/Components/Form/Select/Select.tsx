import _ from 'lodash';
import { Control, useController } from 'react-hook-form';
import { GroupBase, OnChangeValue, Props } from 'react-select';

import UncontrolledSelect, { UncontrolledSelectProps } from './UncontrolledSelect';

export interface SelectProps extends UncontrolledSelectProps {
  control?: Control<any, any>;
}

const Select = <
  Option,
  IsMulti extends boolean = false,
  Group extends GroupBase<Option> = GroupBase<Option>,
>({
  control,
  name,
  ...props
}: Props<Option, IsMulti, Group> & SelectProps) => {
  if (!control || !control.register) {
    return <UncontrolledSelect name={name} {...props} />;
  }

  const {
    field: { value = false, onChange },
  } = useController({
    name,
    control,
  });

  const handleChange = (value: OnChangeValue<Option, IsMulti>) => {
    onChange(value);
  };

  return (
    <UncontrolledSelect
      name={name}
      value={!_.isEmpty(value) ? value : null}
      onChange={handleChange}
      {...props}
    />
  );
};
export default Select;
