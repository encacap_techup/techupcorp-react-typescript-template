import { Control, useController } from 'react-hook-form';

import UncontrolledToggle, { UncontrolledToggleProps } from './UncontrolledToggle';

export type ToggleSizeType = 'xs' | 'default' | 'xl';

export interface ToggleProps extends UncontrolledToggleProps {
  control?: Control<any, any>;
  name?: string;
}

const Toggle = ({ control, name = '', ...props }: ToggleProps) => {
  if (!control || !control.register) {
    return <UncontrolledToggle {...props} />;
  }

  const {
    field: { value = false, onChange },
  } = useController({
    name,
    control,
  });

  return <UncontrolledToggle isOn={value} onChange={onChange} {...props} />;
};
export default Toggle;
