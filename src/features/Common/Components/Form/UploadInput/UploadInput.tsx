import { ChangeEvent, MutableRefObject, useRef } from 'react';
import { AiOutlineReload } from 'react-icons/ai';
import { FiUpload } from 'react-icons/fi';
import { twMerge } from 'tailwind-merge';

import { ImageDataType } from '../../../../../app/Types/Common/commonTypes';
import { getImageURLFromFile } from '../../../Utils/file';

export interface UploadInputProps {
  className?: string;
  files: ImageDataType[];
  multiple: boolean;
  placeholder: string;
  onChange: (files: ImageDataType[] | null) => void;
}

const UploadInput = ({ className, multiple, files, onChange, placeholder }: UploadInputProps) => {
  const inputFileRef = useRef() as MutableRefObject<HTMLInputElement>;

  const handleClickInput = () => {
    const inputFileElement = inputFileRef.current;
    inputFileElement.click();
  };

  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    const filesInput = e.target.files;
    if (filesInput == null) return;
    onChange(Array.from(filesInput) as ImageDataType[]);
  };

  return (
    <button
      type="button"
      className={twMerge(
        'block h-20 w-full cursor-pointer rounded-lg border-2 border-gray-100 p-2 text-left outline-none focus:border-blue-500',
        className,
      )}
      tabIndex={0}
      onClick={handleClickInput}
    >
      <input
        type="file"
        className="hidden"
        name="input_file"
        ref={inputFileRef}
        multiple={multiple}
        onChange={handleChangeInput}
      />
      <div className="flex h-full w-full justify-between">
        {files?.length > 0 ? (
          <div className="flex w-full flex-wrap space-x-4 rounded-lg">
            {Array.from(files).map((file, index) => {
              return (
                <div className="aspect-4/3 h-full w-1/6 overflow-hidden rounded-lg bg-gray-100" key={index}>
                  <img
                    src={getImageURLFromFile(file)}
                    alt="cstorage"
                    className="h-full w-full object-cover object-center"
                  />
                </div>
              );
            })}
            <div className="flex flex-1 items-center justify-center rounded-lg bg-gray-100">
              <AiOutlineReload size={22} />
            </div>
          </div>
        ) : (
          <div className="h-full w-full">
            <div className="grid h-full grid-cols-6 gap-x-2">
              <div className="col-span-5 flex h-full items-center justify-center rounded-lg bg-gray-100 text-inherit">
                {placeholder}
              </div>
              <div className="col-span-1 flex h-full items-center justify-center rounded-lg bg-gray-100 text-inherit">
                <FiUpload size={24} />
              </div>
            </div>
          </div>
        )}
      </div>
    </button>
  );
};
export default UploadInput;
