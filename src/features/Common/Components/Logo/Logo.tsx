import logo from '../../../../assets/images/logo.png';

export interface LogoProps extends React.HTMLAttributes<HTMLDivElement> {
  imageClassName?: string;
}

const Logo = ({ className, imageClassName }: LogoProps) => {
  return (
    <div className={className}>
      <img src={logo} alt="CStorage Logo" className={imageClassName} />
    </div>
  );
};

export default Logo;
