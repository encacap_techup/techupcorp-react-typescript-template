import { Id, toast, ToastOptions } from 'react-toastify';

import ToastMessage from '../Components/ToastMessage/ToastMessage';
/**
 * Basic used of notification.
 * const toast = useToast({ initialConfig});
 * toast.success('Message.');
 * toast.warning('Message.', 'Description.', {
 *   className: 0,
 * });
 */

type ToastType = 'info' | 'success' | 'warning' | 'error';

type ShowFunction = (message: string, description?: string, config?: ToastOptions) => Id;

export interface ShowToastParam {
  type: ToastType;
  message: string;
  description?: string;
  config?: ToastOptions;
}

const useNotify = (initialConfig = { className: 'rounded-xl' }) => {
  const showToast = ({ type, message, description, config }: ShowToastParam) => {
    switch (type) {
      case 'success':
        return toast.success(
          <ToastMessage message={message} description={description} />,
          Object.assign(initialConfig, { className: 'bg-green-50' }, config),
        );
      case 'info':
        return toast.info(
          <ToastMessage message={message} description={description} />,
          Object.assign(initialConfig, { className: 'bg-white' }, config),
        );
      case 'warning':
        return toast.warn(
          <ToastMessage message={message} description={description} />,
          Object.assign(initialConfig, { className: 'bg-yellow-50' }, config),
        );
      case 'error':
        return toast.error(
          <ToastMessage message={message} description={description} />,
          Object.assign(initialConfig, { className: 'bg-red-50' }, config),
        );
      default:
        return toast(
          <ToastMessage message={message} description={description} />,
          Object.assign(initialConfig, { className: 'bg-white' }, config),
        );
    }
  };

  const showSuccess: ShowFunction = (message, description, config) =>
    showToast({ type: 'success', message, description, config });
  const showWarning: ShowFunction = (message, description, config) =>
    showToast({ type: 'warning', message, description, config });
  const showError: ShowFunction = (message, description, config) =>
    showToast({ type: 'error', message, description, config });
  const showInfo: ShowFunction = (message, description, config) =>
    showToast({ type: 'info', message, description, config });

  return {
    success: showSuccess,
    warning: showWarning,
    error: showError,
    info: showInfo,
  };
};

export default useNotify;
