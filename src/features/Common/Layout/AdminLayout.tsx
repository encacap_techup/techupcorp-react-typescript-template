import AdminSidebar from './Components/Sidebar/AdminSidebar';

export interface AdminLayoutProps {
  children: JSX.Element;
}

const AdminLayout = ({ children }: AdminLayoutProps) => {
  return (
    <div className="min-h-fit-layout w-full">
      <AdminSidebar />
      <div className="ml-72 w-fit-layout items-center">{children}</div>
    </div>
  );
};
export default AdminLayout;
