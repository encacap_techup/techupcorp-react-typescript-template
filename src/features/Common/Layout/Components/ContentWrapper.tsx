import { twMerge } from 'tailwind-merge';

import ContentWrapperBody, { ContentWrapperBodyProps } from './ContentWrapperBody';
import ContentWrapperHeader, { ContentWrapperHeaderProps } from './ContentWrapperHeader';

export interface ContentWrapperProps extends ContentWrapperHeaderProps, ContentWrapperBodyProps {
  className?: string;
  isBlank?: boolean;
}

const ContentWrapper = ({ title, className, actions, children, isBlank }: ContentWrapperProps) => {
  return (
    <div className={twMerge('mb-8 w-full px-8', className)}>
      <ContentWrapperHeader title={title} actions={actions} />
      <ContentWrapperBody isBlank={isBlank}>{children}</ContentWrapperBody>
    </div>
  );
};

export default ContentWrapper;
