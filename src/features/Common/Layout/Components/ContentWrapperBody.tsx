import { twMerge } from 'tailwind-merge';

export interface ContentWrapperBodyProps {
  children: React.ReactNode | React.ReactNode[];
  isBlank?: boolean;
}

const ContentWrapperBody = ({ isBlank, children }: ContentWrapperBodyProps) => {
  return (
    <div
      className={twMerge(
        !isBlank && 'rounded-lg border-2 border-gray-100 p-6 shadow shadow-gray-50',
        isBlank && 'border-t-2 border-gray-100',
      )}
    >
      {children}
    </div>
  );
};

export default ContentWrapperBody;
