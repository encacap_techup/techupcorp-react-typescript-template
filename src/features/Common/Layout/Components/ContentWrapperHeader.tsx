import { DefaultTFuncReturn } from 'i18next';

export interface ContentWrapperHeaderProps {
  title?: JSX.Element & DefaultTFuncReturn;
  actions?: JSX.Element;
}

const ContentWrapperHeader = ({ title, actions }: ContentWrapperHeaderProps) => {
  return (
    <div className="flex min-h-[88px] items-center justify-between py-6">
      <div className="text-lg font-semibold">{title}</div>
      {actions && <div className="flex justify-end">{actions}</div>}
    </div>
  );
};

export default ContentWrapperHeader;
