import { useTranslation } from 'react-i18next';

import paypalImage from '../../../../assets/images/footer/paypal.png';
import shape01Image from '../../../../assets/images/footer/shape_01.png';
import Logo from '../../Components/Logo/Logo';
import FooterLinkItem from './FooterLinkItem';
import FooterSocialItems from './FooterSocialItems';

const Footer = () => {
  const { t } = useTranslation(['common'], {
    keyPrefix: 'layout.footer',
  });
  return (
    <div className="relative z-10 mx-auto overflow-hidden border-t-2 border-gray-100 bg-white px-3 sm:px-6 lg:px-8 xl:px-36">
      <div className="absolute left-0 top-4 z-0 h-full w-full">
        <div className="absolute flex h-full w-full">
          <img src={shape01Image} alt="LinkStar" className="h-full w-full object-contain object-center" />
        </div>
      </div>
      <div className="relative z-10 grid gap-x-10 py-4 sm:grid-cols-2 sm:py-6 md:grid-cols-3 md:py-10">
        <div className="">
          <div className="mt-1.5">
            <Logo imageClassName="h-10" />
          </div>
          <div className="mt-6 max-w-xs font-semibold">{t('mailstar')}</div>
          <div className="mt-6 sm:mt-8">
            <img src={paypalImage} alt="Paypal" />
          </div>
        </div>
        <div className="relative z-10 mt-6 grid grid-cols-1 xs:grid-cols-2 sm:mt-0 md:col-span-2 md:grid-cols-3">
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">{t('company')}</div>
            <div className="flex flex-col ">
              <FooterLinkItem to="/">{t('about')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('blog')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('partners')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('contact')}</FooterLinkItem>
            </div>
          </div>
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">{t('support')}</div>
            <div className="flex flex-col ">
              <FooterLinkItem to="/">{t('started')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('docs')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('guides')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('faqs')}</FooterLinkItem>
            </div>
          </div>
          <div className="my-2 flex flex-col sm:my-4">
            <div className="mb-3 font-bold">{t('trustLegal')}</div>
            <div className="flex flex-col ">
              <FooterLinkItem to="/">{t('termsConditions')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('notice')}</FooterLinkItem>
              <FooterLinkItem to="/">{t('claim')}</FooterLinkItem>
            </div>
          </div>
        </div>
      </div>
      <div className="relative z-10 mt-2 items-center justify-between border-t-2 border-gray-100 py-4 md:flex">
        <div className="text-center md:text-left">&copy; {t('reserved')}</div>
        <FooterSocialItems className="mt-4 flex justify-center md:mt-0 md:justify-start" />
      </div>
    </div>
  );
};

export default Footer;
