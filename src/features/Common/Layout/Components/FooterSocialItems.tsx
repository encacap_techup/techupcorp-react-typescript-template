import SocialIcon from '../../Components/SocialIcons';

interface FooterSocialItemsProps {
  className?: string;
}

const FooterSocialItems = ({ className }: FooterSocialItemsProps) => {
  return (
    <div className={className}>
      <div className="mr-4 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full border-2 border-gray-100 bg-white text-blue-500 shadow-md duration-200 hover:bg-blue-500 hover:text-white">
        <SocialIcon hostname="facebook" size={20} />
      </div>
      <div className="mr-4 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full border-2 border-gray-100 bg-white text-primary-500 shadow-md duration-200 hover:bg-primary-500 hover:text-white">
        <SocialIcon hostname="instagram" size={20} />
      </div>
      <div className="mr-4 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full border-2 border-gray-100 bg-white text-cyan-500 shadow-md duration-200 hover:bg-cyan-500 hover:text-white">
        <SocialIcon hostname="twitter" size={20} className="mt-1" />
      </div>
      <div className="mr-4 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full border-2 border-gray-100 bg-white text-blue-500 shadow-md duration-200 last:mr-0 hover:bg-blue-500 hover:text-white">
        <SocialIcon hostname="linkedin" size={20} />
      </div>
    </div>
  );
};

export default FooterSocialItems;
