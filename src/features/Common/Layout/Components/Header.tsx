import _ from 'lodash';
import { useCallback, useLayoutEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { twMerge } from 'tailwind-merge';

import Logo from '../../Components/Logo/Logo';
import useSelector from '../../Hooks/useSelector';
import HeaderLoginButton from './HeaderLoginButton';
import HeaderUserDropdown from './HeaderUserDropdown';

const Header = () => {
  const [isScrollDown, setIsScrollDown] = useState(false);

  const headerRef = useRef<HTMLDivElement>(null);
  const user = useSelector((state) => state.user);
  const headerIncludeScrollEffectPaths: string[] = [];
  const { pathname } = useLocation();

  const handleScroll = useCallback(() => {
    if (!headerIncludeScrollEffectPaths.includes(pathname)) {
      return;
    }
    if (window.scrollY >= 10) {
      setIsScrollDown(true);
    } else {
      setIsScrollDown(false);
    }
  }, [pathname]);

  useLayoutEffect(() => {
    if (headerIncludeScrollEffectPaths.includes(pathname)) {
      setIsScrollDown(false);

      window.addEventListener('scroll', handleScroll);
      return () => window.removeEventListener('scroll', handleScroll);
    }
    setIsScrollDown(true);
  }, [pathname]);

  return (
    <div
      className={twMerge(`sticky top-0 z-50 h-20 w-full ${isScrollDown ? 'bg-white shadow' : 'bg-inherit'}`)}
      ref={headerRef}
    >
      <div className="flex h-full w-full justify-between px-8">
        <Link to="/" className="flex flex-shrink-0 items-center">
          <Logo imageClassName="h-9 w-auto xs:h-10" />
        </Link>
        <div className="flex h-full w-fit items-center">
          {/* <HeaderLanguageSelector /> */}
          {_.isEmpty(user.email) ? <HeaderLoginButton /> : <HeaderUserDropdown />}
        </div>
      </div>
    </div>
  );
};
export default Header;
