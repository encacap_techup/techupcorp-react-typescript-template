import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

const HeaderLoginButton = () => {
  const { t } = useTranslation(['common'], {
    keyPrefix: 'common:layout.header',
  });

  return (
    <NavLink to="/auth/login">
      <button
        type="button"
        className="my-4 justify-center rounded-full border-solid border-black bg-red-600 py-3 px-[30px] text-center text-[12px] font-semibold leading-6 text-white hover:cursor-pointer hover:bg-red-700 focus:bg-red-700 sm:text-base"
      >
        {t('login')}
      </button>
    </NavLink>
  );
};

export default HeaderLoginButton;
