import _ from 'lodash';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FiHelpCircle, FiLogOut, FiSettings } from 'react-icons/fi';
import { Link } from 'react-router-dom';

import { logOut } from '../../../../app/Services/Common/AuthService';
import { clearUser } from '../../../../app/Slices/userSlice';
import useDispatch from '../../Hooks/useDispatch';
import useSelector from '../../Hooks/useSelector';

interface HeaderUserDropdownProps {
  onClick?: () => void;
}

const HeaderUserDropdown = ({ onClick }: HeaderUserDropdownProps) => {
  const { t } = useTranslation(['common'], {
    keyPrefix: 'layout.header.userDropdown',
  });

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isUserDropDownVisible, setUserDropDownVisible] = useState(false);

  const userTabRef = useRef<HTMLDivElement>(null);
  const user = useSelector((state) => state.user);

  const dashboardLink = useMemo(() => {
    if (user?.roles?.some((role) => role.slug === 'system')) return '/system';
    if (user?.roles?.some((role) => role.slug === 'admin')) return '/admin';
    return '/my';
  }, [user]);

  const dispatch = useDispatch();

  const handleClickLogout = () => {
    if (isSubmitting) {
      return;
    }
    setIsSubmitting(true);

    logOut()
      .then(() => {
        dispatch(clearUser());
        window.location.reload();
      })
      .catch(() => {
        setIsSubmitting(false);
      });
  };

  const handleClick = () => {
    onClick?.();
    setUserDropDownVisible(false);
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (userTabRef.current && !userTabRef.current.contains(event.target as Node)) {
        setUserDropDownVisible(false);
      }
    };

    document.addEventListener('click', handleClickOutside, true);

    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, [userTabRef]);

  return (
    <div className="relative h-10 w-10 rounded-full shadow-md" ref={userTabRef}>
      <div
        role="button"
        tabIndex={0}
        className="h-full w-full overflow-hidden rounded-full"
        onClick={() => setUserDropDownVisible((prev) => !prev)}
      >
        <img className="h-full w-full rounded-full" src={user?.avatar_img} alt="" />
      </div>
      {isUserDropDownVisible && (
        <div className="absolute top-12 right-0 w-48 rounded-lg border-2 border-gray-100 bg-white p-4 text-slate-700 shadow-lg shadow-gray-100 transition duration-100 ease-linear">
          <div className="group flex w-full cursor-pointer" role="button" tabIndex={0} onClick={handleClick}>
            <div className="mb-3 h-full w-full border-b-2 border-gray-100 pb-5 text-sm">
              <Link to={dashboardLink} className="font-semibold group-hover:text-primary-500">
                {_.capitalize(user.first_name)} {_.capitalize(user.last_name)}
              </Link>
              <Link
                to={dashboardLink}
                className="mt-1 break-all text-sm line-clamp-1 group-hover:text-primary-500"
              >
                {user.email}
              </Link>
            </div>
          </div>
          <div
            className="mt-1.5 flex w-full items-center hover:text-red-600"
            role="button"
            tabIndex={0}
            onClick={handleClick}
          >
            <FiSettings />
            <p className="ml-3 text-sm">
              <Link to={dashboardLink}>{t('setting')}</Link>
            </p>
          </div>
          <div
            className="mt-2 flex w-full items-center hover:text-red-600"
            role="button"
            tabIndex={0}
            onClick={handleClick}
          >
            <FiHelpCircle />
            <Link to="help">
              <p className="ml-3 text-sm">{t('helpAndContact')}</p>
            </Link>
          </div>
          <div
            className="mt-5 flex w-full items-center border-t-2 border-gray-100 pt-3 hover:text-red-600"
            role="button"
            tabIndex={0}
            onClick={handleClickLogout}
          >
            {isSubmitting ? (
              <div className="h-4 w-4 animate-spin rounded-full border border-slate-700 border-t-transparent" />
            ) : (
              <FiLogOut />
            )}
            <div className="ml-3 text-sm">{t('logOut')}</div>
          </div>
        </div>
      )}
    </div>
  );
};
export default HeaderUserDropdown;
