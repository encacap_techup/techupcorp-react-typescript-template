import { useTranslation } from 'react-i18next';
import { MdOutlineDashboardCustomize } from 'react-icons/md';

import Sidebar from './Sidebar.js';
import SidebarItem from './SidebarItem.js';

const AdminSidebar = () => {
  const { t } = useTranslation(['admin'], {
    keyPrefix: 'layout.sidebar',
  });

  return (
    <Sidebar>
      <SidebarItem to="/admin" icon={<MdOutlineDashboardCustomize />} text={t('dashboard')} />
    </Sidebar>
  );
};

export default AdminSidebar;
