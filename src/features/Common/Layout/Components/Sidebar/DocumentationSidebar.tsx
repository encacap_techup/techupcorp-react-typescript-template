import { useTranslation } from 'react-i18next';
import { AiOutlineNotification } from 'react-icons/ai';
import { BiTable } from 'react-icons/bi';
import { BsToggleOn } from 'react-icons/bs';
import { CgFormatLeft, CgSelectR } from 'react-icons/cg';
import { MdOutlineUpload } from 'react-icons/md';

import Sidebar from './Sidebar';
import SidebarGroup from './SidebarGroup';
import SidebarItem from './SidebarItem';

const DocumentationSidebar = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'layout.sidebar',
  });

  return (
    <Sidebar>
      <SidebarItem to="/docs/table" icon={<BiTable />} text={t('table')} />
      <SidebarGroup title={t('formGroup')} className="mb-5 pb-5">
        <SidebarItem to="/docs/form" icon={<CgFormatLeft />} text={t('form')} />
        <SidebarItem to="/docs/select" icon={<CgSelectR />} text={t('select')} />
        <SidebarItem to="/docs/upload-input" icon={<MdOutlineUpload />} text={t('uploadInput')} />
      </SidebarGroup>
      <SidebarItem to="/docs/notify" icon={<AiOutlineNotification />} text={t('notify')} />
      <SidebarItem to="/docs/toggle" icon={<BsToggleOn />} text={t('toggle')} />
    </Sidebar>
  );
};

export default DocumentationSidebar;
