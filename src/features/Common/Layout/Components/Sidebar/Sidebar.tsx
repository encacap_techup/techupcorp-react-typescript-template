import { twMerge } from 'tailwind-merge';

export interface SidebarProps {
  children: React.ReactNode | React.ReactNode[];
  className?: string;
}

const Sidebar = ({ children, className = '' }: SidebarProps) => {
  return (
    <div className={twMerge(' fixed top-0 left-0 z-10 h-full w-72 overflow-hidden pt-20 ', className)}>
      <div className="h-full w-full overflow-hidden border-r-2 border-gray-100 bg-gray-50 py-5 px-4 scrollbar-hide hover:overflow-clip hover:overflow-y-auto">
        {children}
      </div>
    </div>
  );
};

export default Sidebar;
