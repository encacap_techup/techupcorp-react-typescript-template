import _ from 'lodash';
import { Children, ReactElement, useEffect, useState } from 'react';
import { BiChevronRight } from 'react-icons/bi';
import { useLocation } from 'react-router-dom';
import { twMerge } from 'tailwind-merge';

export interface SidebarGroupProps {
  title: string;
  isActive?: boolean;
  children: React.ReactNode | React.ReactNode[];
  className?: string;
}

const SidebarGroup = ({ children, title, isActive, className }: SidebarGroupProps) => {
  const [isShow, setIsShow] = useState(false);

  const childrenPath = (Children.toArray(children) as ReactElement[]).map(
    (item) => item && 'props' in item && item.props.to,
  );

  const location = useLocation();

  useEffect(() => {
    if (isActive ?? _.includes(childrenPath, location.pathname)) {
      setIsShow(true);
    }
  }, [isActive]);

  return (
    <div
      className={twMerge(
        'relative h-fit w-full py-4 after:absolute after:bottom-0 after:left-6 after:h-0.5 after:w-52 after:bg-gray-200',
        isShow && 'pb-0',
        className,
      )}
    >
      <div
        className={twMerge(
          'flex h-4 w-full text-xs font-semibold text-gray-500 hover:text-primary-600',
          isShow && 'mb-4',
        )}
        role="button"
        tabIndex={0}
        onClick={() => setIsShow((prev) => !prev)}
      >
        <div>
          <BiChevronRight
            className={twMerge('-ml-0.5 mr-[3px] text-base duration-200', isShow && 'rotate-90')}
          />
        </div>
        <div className="ml-2">{title}</div>
      </div>
      {isShow && children}
    </div>
  );
};

export default SidebarGroup;
