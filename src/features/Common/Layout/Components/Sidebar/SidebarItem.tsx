import { cloneElement, useMemo } from 'react';
import { Link, useLocation } from 'react-router-dom';

export interface SidebarItemProps {
  to?: string;
  icon: JSX.Element;
  text: string;
}

const SidebarItem = ({ to, icon, text }: SidebarItemProps) => {
  const { pathname } = useLocation();

  const isActivated = useMemo(() => {
    if (pathname === to) {
      return true;
    }
    return false;
  }, [pathname, to]);

  return (
    <Link
      to={to ?? ''}
      className={`group relative my-1 flex items-center py-3 px-4 font-[450] before:absolute before:top-1/2 before:left-0 before:h-6 before:w-1 before:-translate-y-1/2 before:rounded-sm before:bg-primary-800 hover:rounded-md hover:bg-gray-200 hover:text-primary-600 xs:px-7 md:px-6 ${
        isActivated ? 'rounded-md bg-gray-200 text-primary-600 before:block' : 'text-slate-700 before:hidden'
      }`}
    >
      {cloneElement(icon, {
        className: 'flex-shrink-0 w-5 mr-4 group-hover:text-primary-600 ml-0.5 xs:ml-0',
        size: 20,
      })}
      <span className="hidden md:inline-block">{text}</span>
    </Link>
  );
};

export default SidebarItem;
