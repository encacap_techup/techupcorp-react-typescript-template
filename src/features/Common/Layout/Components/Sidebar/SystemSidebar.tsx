import { useTranslation } from 'react-i18next';
import { AiOutlineFile } from 'react-icons/ai';
import { BiExtension } from 'react-icons/bi';
import { FiFolder, FiSettings, FiUserCheck, FiUsers } from 'react-icons/fi';
import { MdOutlineDashboardCustomize, MdOutlineWeb } from 'react-icons/md';
import { RiFolderOpenFill } from 'react-icons/ri';

import Sidebar from './Sidebar';
import SidebarItem from './SidebarItem';

const SystemSidebar = () => {
  const { t } = useTranslation(['system'], {
    keyPrefix: 'layout.sidebar',
  });

  return (
    <Sidebar>
      <SidebarItem to="/system" icon={<MdOutlineDashboardCustomize />} text={t('dashboard')} />
      <SidebarItem to="/system/folders" icon={<FiFolder />} text={t('folder')} />
      <SidebarItem to="/system/files" icon={<AiOutlineFile />} text={t('file')} />
      <SidebarItem to="/system/websites" icon={<MdOutlineWeb />} text={t('website')} />
      <SidebarItem to="/system/configs" icon={<FiSettings />} text={t('config')} />
      <SidebarItem to="/system/file-types" icon={<BiExtension />} text={t('fileExtensions')} />
      <SidebarItem to="/system/users" icon={<FiUsers />} text={t('user')} />
      <SidebarItem to="/system/roles" icon={<FiUserCheck />} text={t('role')} />
      <SidebarItem to="/system/disk" icon={<RiFolderOpenFill />} text={t('disk')} />
    </Sidebar>
  );
};

export default SystemSidebar;
