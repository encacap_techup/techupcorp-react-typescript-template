import { HiFolderDownload } from 'react-icons/hi';

import Sidebar from './Sidebar';
import SidebarItem from './SidebarItem';

const UserSidebar = () => {
  return (
    <Sidebar>
      <SidebarItem text="Quick Access" icon={<HiFolderDownload />} />
    </Sidebar>
  );
};

export default UserSidebar;
