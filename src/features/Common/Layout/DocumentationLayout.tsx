import DocumentationSidebar from './Components/Sidebar/DocumentationSidebar';

interface DocumentationLayoutProps {
  children: React.ReactNode;
}

const DocumentationLayout = ({ children }: DocumentationLayoutProps) => {
  return (
    <div className="w-full">
      <DocumentationSidebar />
      <div className="ml-72 min-h-fit-layout w-fit-layout items-center">{children}</div>
    </div>
  );
};
export default DocumentationLayout;
