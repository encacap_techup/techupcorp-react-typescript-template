import SystemSidebar from './Components/Sidebar/SystemSidebar';

export interface SystemLayoutProps {
  children: JSX.Element;
}

const SystemLayout = ({ children }: SystemLayoutProps) => {
  return (
    <div className="min-h-fit-layout w-full">
      <SystemSidebar />
      <div className="ml-72 w-fit-layout items-center">{children}</div>
    </div>
  );
};
export default SystemLayout;
