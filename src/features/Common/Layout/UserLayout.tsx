import UserSidebar from './Components/Sidebar/UserSidebar';

interface UserLayoutProps {
  children: JSX.Element;
}

const UserLayout = ({ children }: UserLayoutProps) => {
  return (
    <div className="min-h-fit-layout w-full">
      <UserSidebar />
      <div className="ml-72 min-h-fit-layout w-fit-layout">{children}</div>
    </div>
  );
};
export default UserLayout;
