// This is a common schema that can be used in any feature.
// If a shared schema is needed, it should be placed here.
// If not, it should be placed in the feature's schema folder.

import { object, string } from 'yup';

const exampleSchema = (translation: Record<string, string>) =>
  object().shape({
    example: object().shape({
      example: string().required(translation?.example),
    }),
  });

export default exampleSchema;
