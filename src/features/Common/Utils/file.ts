import { ImageDataType } from '../../../app/Types/Common/commonTypes';

const getImageURLFromFile = (file: ImageDataType) => {
  if (file instanceof File) {
    return URL.createObjectURL(file);
  }

  return file.absolute_slug;
};

export { getImageURLFromFile };
