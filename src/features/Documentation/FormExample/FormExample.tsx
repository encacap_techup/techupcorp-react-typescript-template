import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useLayoutEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { SelectOptionType } from '../../../app/Types/Common/elementTypes';
import { FormExampleFormDataType } from '../../../app/Types/Documentation/formExample';
import Button from '../../Common/Components/Button';
import { Checkbox, Input } from '../../Common/Components/Form';
import { Select } from '../../Common/Components/Form/Select';
import Toggle from '../../Common/Components/Form/Toggle/Toggle';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';
import { setDocumentTitle } from '../../Common/Utils/helpers';
import formExampleSchema from './Schemas/formExampleSchema';

const FormExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.form',
  });

  const [isSubmitting, setIsSubmitting] = useState(false);

  const defaultValue: FormExampleFormDataType = {
    name: 'This is a default value',
    email: '',
    enableSomething: false,
    enableWhenLoaded: true,
    otherEnableSomething: false,
    otherEnableWhenLoaded: true,
    gender: null,
    multipleSelect: [],
  };

  const {
    handleSubmit: useFormSubmit,
    formState: { errors },
    control,
    watch,
    setValue,
    reset,
  } = useForm<FormExampleFormDataType>({
    defaultValues: defaultValue,
    resolver: yupResolver(formExampleSchema(t)),
  });

  const options: SelectOptionType[] = [
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },
  ];

  // Watch for changes in the form
  const enableSomething = watch('enableSomething', false);
  const otherEnableSomething = watch('otherEnableSomething', false);
  const gender = watch('gender', null);
  const multipleSelect = watch('multipleSelect', []);

  const handleSubmit = useFormSubmit((data) => {
    setIsSubmitting(true);
    console.log('FormData', data);
    // ... do something with the data
  });

  const handleResetForm = () => {
    reset(defaultValue);
  };

  useEffect(() => {
    console.groupCollapsed('FormExample');
    console.log('Watching enableSomething:', enableSomething);
    console.log('Watching otherEnableSomething:', otherEnableSomething);
    console.log('Watching gender:', gender);
    console.log('Watching multipleSelect:', multipleSelect);
    console.groupEnd();
  }, [enableSomething, otherEnableSomething, gender, multipleSelect]);

  // Set the value of the field `enableWhenLoaded` to true when the component is mounted
  useEffect(() => {
    setValue('enableWhenLoaded', true);
    setValue('otherEnableWhenLoaded', true);
  }, [setValue]);

  useLayoutEffect(() => {
    setDocumentTitle(t('title'));
  }, [t]);

  return (
    <ContentWrapper title={t('title')} className="min-h-screen">
      <form className="grid grid-cols-1 gap-6" onSubmit={handleSubmit}>
        <Input
          name="name"
          label={t('form.name.label')}
          control={control}
          className="block w-full"
          error={errors.name?.message}
        />
        <Input
          name="email"
          label={t('form.email.label')}
          control={control}
          className="block w-full"
          error={errors.email?.message}
        />
        <Select
          name="gender"
          control={control}
          options={options}
          placeholder={t('form.gender.placeholder')}
          className="text-normal"
          error={errors.gender?.message}
        />
        <Select
          name="multipleSelect"
          control={control}
          options={options}
          placeholder={t('form.multipleSelect.placeholder')}
          className="text-normal"
          error={errors.multipleSelect?.message}
          isMulti
        />
        <div className="flex flex-col space-y-4">
          <Toggle
            name="enableSomething"
            control={control}
            label={t('form.enableSomething.label')}
            error={errors.enableSomething?.message}
          />
          <Toggle name="enableWhenLoaded" control={control} label={t('form.enableWhenLoaded.label')} />
        </div>
        <div className="-mt-2 space-y-4">
          <Checkbox
            name="otherEnableSomething"
            label={t('form.enableSomething.label')}
            control={control}
            className="flex space-x-9"
            error={errors.otherEnableSomething?.message}
          />
          <Checkbox
            name="otherEnableWhenLoaded"
            label={t('form.otherEnableWhenLoaded.label')}
            control={control}
            className="flex space-x-9"
          />
        </div>
        <div className="flex items-center justify-center space-x-6">
          <Button type="button" color="light" size="sm" onClick={handleResetForm} disabled={isSubmitting}>
            {t('action.reset')}
          </Button>
          <Button
            type="submit"
            size="sm"
            className="w-full rounded-md"
            disabled={isSubmitting}
            isLoading={isSubmitting}
            // TODO: only use onClick when there is no form.
            // onClick={handleSubmit}
          >
            {t('action.submit')}
          </Button>
        </div>
      </form>
    </ContentWrapper>
  );
};

export default FormExample;
