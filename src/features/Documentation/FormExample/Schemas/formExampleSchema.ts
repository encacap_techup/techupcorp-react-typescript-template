import { TFunction } from 'i18next';
import { boolean, object, string } from 'yup';

// The parameters `nameTranslation`, `emailTranslation` are the translations for the required fields.
// There is no specific convention for those translations, pass them as you wish.
const formExampleSchema = (translation: TFunction) =>
  object().shape({
    name: string().required(translation('form.name.required') ?? ''),
    email: string()
      .required(translation('form.email.required') ?? '')
      .email(translation('form.email.invalid') ?? ''),
    enableSomething: boolean().isTrue(translation('form.enableSomething.required') ?? ''),
    otherEnableSomething: boolean().isTrue(translation('form.otherEnableSomething.required') ?? ''),
    gender: object()
      .nullable()
      .required(translation('form.gender.required') ?? ''),
  });

export default formExampleSchema;
