import { useTranslation } from 'react-i18next';

import Button from '../../Common/Components/Button';
import useNotify from '../../Common/Hooks/useNotify';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';

const NotifyExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.notify',
  });

  const toast = useNotify();

  const handleClickShowSuccess = () => {
    toast.success(t('success'));
  };

  const handleClickShowError = () => {
    toast.error(t('error'));
  };

  const handleClickShowWarning = () => {
    toast.warning(t('warning'));
  };

  const handleClickShowDefault = () => {
    toast.info(t('default'));
  };
  return (
    <ContentWrapper title={t('title')} className="min-h-screen">
      <div className="grid grid-cols-5 gap-4">
        <Button onClick={handleClickShowSuccess}>{t('show.success')}</Button>
        <Button onClick={handleClickShowError}>{t('show.error')}</Button>
        <Button onClick={handleClickShowWarning}>{t('show.warning')}</Button>
        <Button onClick={handleClickShowDefault}>{t('show.default')}</Button>
      </div>
    </ContentWrapper>
  );
};
export default NotifyExample;
