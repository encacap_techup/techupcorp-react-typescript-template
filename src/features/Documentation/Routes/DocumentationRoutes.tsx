import { Route, Routes } from 'react-router-dom';

import DocumentationLayout from '../../Common/Layout/DocumentationLayout';
import FormExample from '../FormExample/FormExample';
import NotifyExample from '../NotifyExample/NotifyExample';
import SelectExample from '../SelectExample/SelectExample';
import TableExample from '../TableExample/TableExample';
import ToggleExample from '../ToggleExample/ToggleExample';
import UploadInputExample from '../UploadInputExample/UploadInputExample';

const DocumentationRoutes = () => {
  return (
    <DocumentationLayout>
      <Routes>
        <Route
          path="*"
          element={
            <div className="flex min-h-fit-layout w-full items-center justify-center">Documentation</div>
          }
        />
        <Route path="table" element={<TableExample />} />
        <Route path="select" element={<SelectExample />} />
        <Route path="upload-input" element={<UploadInputExample />} />
        <Route path="notify" element={<NotifyExample />} />
        <Route path="toggle-button" element={<ToggleExample />} />
        <Route path="form" element={<FormExample />} />
      </Routes>
    </DocumentationLayout>
  );
};

export default DocumentationRoutes;
