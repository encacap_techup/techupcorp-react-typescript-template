import _ from 'lodash';
import { useLayoutEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MultiValue, SingleValue } from 'react-select';

import { SelectOptionType } from '../../../app/Types/Common/elementTypes';
import { Input } from '../../Common/Components/Form';
import { Select } from '../../Common/Components/Form/Select';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';
import { setDocumentTitle } from '../../Common/Utils/helpers';

const SelectExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.select',
  });

  const options: SelectOptionType[] = [
    { value: '1', label: 'Demo 1' },
    { value: '2', label: 'Option 2' },
    { value: '3', label: 'Option 3' },
    { value: '4', label: 'Option 4' },
    { value: '5', label: 'Option 5' },
    { value: '6', label: 'Option 6' },
    { value: '7', label: 'Option 7' },
    { value: '8', label: 'Option 8' },
    { value: '9', label: 'Option 9' },
  ];

  const [valueA, setValueA] = useState<SingleValue<SelectOptionType>>();
  const [valueB, setValueB] = useState<MultiValue<SelectOptionType>>();

  const onChangeMulti = (value: MultiValue<SelectOptionType>) => {
    setValueB(_.size(value) ? value : undefined);
  };

  const onChange = (value: SingleValue<SelectOptionType>) => {
    setValueA(_.size(value) ? value : undefined);
  };

  useLayoutEffect(() => {
    setDocumentTitle(t('title'));
  }, [t]);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleChangeInput = (value: any) => {
    // eslint-disable-next-line no-console
    console.log(value);
  };

  return (
    <ContentWrapper title={t('title')} className="min-h-screen">
      <div className="grid grid-cols-2 gap-4">
        <div className="col-span-2">{t('searchSelect.title')}</div>
        <div>
          <Select
            name="select"
            options={options}
            placeholder={t('searchSelect.placeholder')}
            value={valueA}
            className="text-normal"
            onChange={onChange}
            onInputChange={handleChangeInput}
          />
        </div>
        <Input name="input" id="fullName" label={t('searchSelect.placeholder')} />
        <div className="col-span-2">{t('searchSelect.title')}</div>
        <div>
          <Select
            name="select"
            options={options}
            placeholder={t('searchSelect.placeholder')}
            value={valueA}
            className="text-normal"
            onChange={onChange}
            onInputChange={handleChangeInput}
            error="Phu"
          />
        </div>
        <Input name="inputWithError" id="fullName" label={t('searchSelect.placeholder')} error="Phu" />
        <div className="col-span-2">{t('searchSelect.title')}</div>
        <div>
          <Select
            name="select"
            options={options}
            placeholder={t('searchSelect.placeholder')}
            value={valueA}
            className="text-normal"
            onChange={onChange}
            onInputChange={handleChangeInput}
            isDisabled
          />
        </div>
        <Input name="inputWithValueDisabled" id="fullName" label={t('searchSelect.placeholder')} disabled />
        <div className="col-span-2 mt-4">{t('multipleSelect.title')}</div>
        <div>
          <Select
            name="multipleSelect"
            isMulti
            options={options}
            placeholder={t('multipleSelect.placeholder')}
            onChange={onChangeMulti}
            value={valueB}
            className="text-normal"
          />
        </div>
        <Input
          name="inputWithValue"
          label={t('multipleSelect.placeholder')}
          value={t('multipleSelect.placeholder') ?? ''}
          readOnly
        />
      </div>
    </ContentWrapper>
  );
};
export default SelectExample;
