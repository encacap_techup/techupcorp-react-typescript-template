import { createColumnHelper } from '@tanstack/react-table';

import { DocumentationTableExampleService } from '../../../../app/Services';
import { ColumnDef, TableRowActionClickHandlerType } from '../../../../app/Types/Common/elementTypes';
import { UserDataType } from '../../../../app/Types/Common/userTypes';
import TableExampleImageColumn from '../Components/TableExampleImageColumn';
import TableExampleImageColumnSkeleton from '../Components/TableExampleImageColumnSkeleton';
import TableExampleRowActions from '../Components/TableExampleRowActions';
import TableExampleRowActionsSkeleton from '../Components/TableExampleRowActionsSkeleton';

interface OnClickHandlers {
  onClickEdit: TableRowActionClickHandlerType;
  onClickDelete: TableRowActionClickHandlerType;
}

const createTableExampleColumns = ({ onClickEdit, onClickDelete }: OnClickHandlers) => {
  const columnHelper = createColumnHelper<UserDataType>();

  const tableExampleColumns: Array<ColumnDef<UserDataType>> = [
    columnHelper.accessor((row) => row.avatar_img, {
      id: 'avatar_img_absolute',
      header: 'Avatar',
      enableSorting: false,
      cell: (info) => <TableExampleImageColumn src={info.getValue()} alt={info.getValue()} />,
      meta: {
        skeleton: <TableExampleImageColumnSkeleton />,
      },
    }),
    columnHelper.accessor((row) => row.first_name, {
      id: 'first_name',
      header: 'First Name',
      meta: {
        filterBy: ['first_name', 'last_name'],
        filterLabel: 'Full Name',
        getFilterOptions: DocumentationTableExampleService.getTableExampleData,
      },
    }),
    columnHelper.accessor((row) => row.last_name, {
      id: 'last_name',
      header: 'Last Name',
    }),
    columnHelper.accessor((row) => row.email, {
      id: 'email',
      header: 'Email',
      meta: {
        filterBy: 'email',
        getFilterOptions: DocumentationTableExampleService.getTableExampleData,
      },
    }),
    columnHelper.display({
      id: 'actions',
      cell: (props) => (
        <TableExampleRowActions
          id={props.row.original.uuid}
          onClickEdit={onClickEdit}
          onClickDelete={onClickDelete}
        />
      ),
      meta: {
        skeleton: <TableExampleRowActionsSkeleton />,
      },
    }),
  ];

  return tableExampleColumns;
};

export default createTableExampleColumns;
