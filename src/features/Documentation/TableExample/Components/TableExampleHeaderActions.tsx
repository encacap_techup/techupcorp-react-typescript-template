import { useTranslation } from 'react-i18next';
import { BiUserPlus } from 'react-icons/bi';

import Button from '../../../Common/Components/Button';

const TableExampleHeaderActions = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.table.header.actions',
  });

  return (
    <Button className="rounded-md shadow-none" size="sm">
      <BiUserPlus size={24} className="mr-2" />
      {t('addUser')}
    </Button>
  );
};

export default TableExampleHeaderActions;
