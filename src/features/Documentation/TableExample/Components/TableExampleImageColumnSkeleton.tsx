import LoadingSkeleton from '../../../Common/Components/Loading/LoadingSkeleton';

const TableExampleImageColumnSkeleton = () => {
  return <LoadingSkeleton className="mx-auto h-12 w-12 rounded-full" />;
};

export default TableExampleImageColumnSkeleton;
