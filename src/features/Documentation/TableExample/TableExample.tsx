import { RowSelectionState, SortingState } from '@tanstack/react-table';
import { useEffect, useLayoutEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentationTableExampleService } from '../../../app/Services';
import { TablePaginationType } from '../../../app/Types/Common/commonTypes';
import { TableColumnFiltersState } from '../../../app/Types/Common/elementTypes';
import { UserDataType } from '../../../app/Types/Common/userTypes';
import { ConfirmationModal } from '../../Common/Components/Modal';
import Table from '../../Common/Components/Table/Table';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';
import { setDocumentTitle } from '../../Common/Utils/helpers';
import createTableExampleColumns from './Columns/TableExampleColumns';
import TableExampleHeaderActions from './Components/TableExampleHeaderActions';

const TableExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.table',
  });

  const [userData, setUserData] = useState<UserDataType[]>([]);
  const [pagination, setPagination] = useState<TablePaginationType>({
    pageIndex: 1,
    pageSize: 10,
  });
  const [sorting, setSorting] = useState<SortingState>([]);
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});
  const [columnFilters, setColumnFilters] = useState<TableColumnFiltersState>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [, setSelectedUser] = useState<UserDataType | null>(null);

  const handleClickEditButton = (id: number | undefined) => {
    // eslint-disable-next-line no-console
    console.log('Edit button clicked', id);
  };

  const handleClickDeleteButton = (id: number | undefined) => {
    const user = userData.find((item) => item.uuid === id);
    if (!user) {
      return;
    }
    setSelectedUser(user);
    setIsShowDeleteModal(true);
  };

  useLayoutEffect(() => {
    setDocumentTitle(t('title'));
  }, [t]);

  useEffect(() => {
    DocumentationTableExampleService.getTableExampleData()
      .then((response) => {
        setUserData(response.data);
        setPagination(() => ({
          pageIndex: response.meta?.current_page,
          pageSize: response.meta?.per_page,
          totalRows: response.meta?.total,
          totalPages: response.meta?.last_page,
        }));
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  /* eslint-disable no-console */
  useEffect(() => {
    console.groupCollapsed('TableExampleState');
    console.log('TableExample Pagination:', pagination);
    console.log('TableExample Sorting:', sorting);
    console.log('TableExample ColumnFilters:', columnFilters);
    console.log('TableExample Selection:', rowSelection);
    console.groupEnd();
  }, [pagination, sorting, columnFilters, rowSelection]);
  /* eslint-enable no-console */

  return (
    <ContentWrapper title={t('title')} actions={<TableExampleHeaderActions />}>
      <Table
        data={userData}
        columns={createTableExampleColumns({
          onClickEdit: handleClickEditButton,
          onClickDelete: handleClickDeleteButton,
        })}
        pagination={pagination}
        sorting={sorting}
        columnFilters={columnFilters}
        rowSelection={rowSelection}
        isLoading={isLoading}
        onChangePagination={setPagination}
        onChangeSorting={setSorting}
        onChangeFilters={setColumnFilters}
        onChangeRowSelection={setRowSelection}
      />
      <ConfirmationModal
        title="Bạn có chắc muốn xoá người dùng này?"
        message='Người dùng "Nguyễn Văn A" sẽ bị xoá khỏi hệ thống. Thao tác này không thể hoàn tác.'
        isOpen={isShowDeleteModal}
        status="danger"
        onClose={() => setIsShowDeleteModal(false)}
        onConfirm={() => setIsShowDeleteModal(true)}
      />
    </ContentWrapper>
  );
};

export default TableExample;
