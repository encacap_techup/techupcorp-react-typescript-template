import { useTranslation } from 'react-i18next';

import Toggle from '../../Common/Components/Form/Toggle/Toggle';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';

const ToggleExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.toggle',
  });
  const handleChangeToggle = (value: boolean) => {
    /* eslint-disable no-console */
    console.log(value);
  };
  return (
    <ContentWrapper title={t('title')} className="min-h-screen">
      <div className="grid grid-cols-1 gap-2">
        <Toggle isOn size="xs" onChange={handleChangeToggle} />
        <Toggle onChange={handleChangeToggle} />
        <Toggle size="xl" onChange={handleChangeToggle} />
      </div>
    </ContentWrapper>
  );
};
export default ToggleExample;
