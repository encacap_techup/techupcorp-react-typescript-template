import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ImageDataType } from '../../../app/Types/Common/commonTypes';
import { UploadInput } from '../../Common/Components/Form';
import ContentWrapper from '../../Common/Layout/Components/ContentWrapper';

const UploadInputExample = () => {
  const { t } = useTranslation(['documentation'], {
    keyPrefix: 'pages.uploadInput',
  });
  const [singleImg, setSingleImg] = useState<ImageDataType[]>([]);
  const [multiImg, setMultiImg] = useState<ImageDataType[]>([]);
  const handleEditSingleImg = () => {
    setSingleImg([
      {
        slug: 'Cstorage-image',
        absolute_slug:
          'https://file.storage.techupzone.com/linkstar-stg/public/upload/534ad792-ba03-41a0-9b96-fc3f9894cd49_1664330071.png',
      },
    ]);
  };
  const handleEditMultiImg = () => {
    setMultiImg([
      {
        slug: 'Cstorage-image',
        absolute_slug:
          'https://file.storage.techupzone.com/linkstar-stg/public/upload/534ad792-ba03-41a0-9b96-fc3f9894cd49_1664330071.png',
      },
      {
        slug: 'Cstorage-image',
        absolute_slug:
          'https://file.storage.techupzone.com/linkstar-stg/public/upload/534ad792-ba03-41a0-9b96-fc3f9894cd49_1664330071.png',
      },
    ]);
  };
  const handleChangeInput = (data: ImageDataType[] | null) => {
    if (data) {
      setSingleImg(data);
    }
  };
  const handleChangeInputMulti = (data: ImageDataType[] | null) => {
    if (data) {
      setMultiImg(data);
    }
  };
  return (
    <ContentWrapper title={t('title')} className="min-h-screen">
      <div className="grid grid-cols-2 gap-4">
        <div className="col-span-1">
          <div className="my-4">{t('singleImg')}</div>
          <UploadInput
            multiple={false}
            placeholder={t('choseSingle')}
            files={singleImg}
            onChange={handleChangeInput}
          />
          <div className="my-2 grid h-10 grid-cols-2 gap-2">
            <div
              className="col-span-1 flex h-full items-center justify-center rounded-lg border-2 border-gray-200 bg-gray-100 hover:bg-gray-200"
              onClick={handleEditSingleImg}
              role="button"
              tabIndex={0}
            >
              {t('edit')}
            </div>
            <div
              className="col-span-1 flex h-full items-center justify-center rounded-lg border-2 border-gray-200 bg-gray-100 hover:bg-gray-200"
              onClick={() => setSingleImg([])}
              role="button"
              tabIndex={0}
            >
              {t('clear')}
            </div>
          </div>
        </div>
        <div className="col-span-1">
          <div className="my-4">{t('multiImg')}</div>
          <UploadInput
            multiple
            placeholder={t('choseMulti')}
            files={multiImg}
            onChange={handleChangeInputMulti}
          />
          <div className="my-2 grid h-10 grid-cols-2 gap-2">
            <div
              className="col-span-1 mb-2 flex  h-full items-center justify-center rounded-lg border-2 border-gray-200 bg-gray-100 hover:bg-gray-200"
              onClick={handleEditMultiImg}
              role="button"
              tabIndex={0}
            >
              {t('edit')}
            </div>
            <div
              className="col-span-1 flex h-full items-center justify-center rounded-lg border-2 border-gray-200 bg-gray-100 hover:bg-gray-200"
              onClick={() => setMultiImg([])}
              role="button"
              tabIndex={0}
            >
              {t('clear')}
            </div>
          </div>
        </div>
      </div>
    </ContentWrapper>
  );
};

export default UploadInputExample;
