import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { setDocumentTitle } from '../Common/Utils/helpers';

const Home = () => {
  const { t } = useTranslation(['common'], {
    keyPrefix: 'pages.home',
  });

  useEffect(() => {
    setDocumentTitle(t('title'));
  }, [t]);

  return <div className="flex h-fit-layout w-full items-center justify-center">{t('title')}</div>;
};

export default Home;
