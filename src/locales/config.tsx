import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import adminEn from './en/admin.json';
import commonEn from './en/common.json';
import documentationEn from './en/documentation.json';
import systemEn from './en/system.json';
import userEn from './en/user.json';

export const resources = {
  en: {
    documentation: documentationEn,
    admin: adminEn,
    common: commonEn,
    user: userEn,
    system: systemEn,
  },
};

i18n
  .use(initReactI18next)
  .init({
    lng: 'en',
    ns: ['admin', 'user', 'common', 'system'],
    interpolation: {
      escapeValue: false,
    },
    resources,
  })
  .catch((error) => {
    console.error(error);
  });
